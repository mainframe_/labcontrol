﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace LabControl.Models
{
    public class Comandos
    {
        Conexao c = new Conexao();
        SqlCommand cmd = new SqlCommand();
        public string msg = "@mg";
        public void cadastrarAluno(int ra, String nome, String email)
        {
            c.conectar();
            cmd.CommandText = "exec cadastroAluno @ra, @nome, @email, @mg output";

            cmd.Parameters.AddWithValue("@ra", ra);
            cmd.Parameters.AddWithValue("@nome", nome);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@mg", msg);

            try
            {
                cmd.Connection = c.conectar();
                cmd.ExecuteNonQuery();
                msg = "Cadastrado com sucesso!";
                c.encerrar();
            }
            catch(SqlException e)
            {
                msg = "Erro ao conectar-se";
                throw e;
            }

            
        }

        public Usuario Autenticar(string login, string senha)
        {
            c.conectar();
            cmd.Parameters.Clear();
            cmd.CommandText = @"select * from usuario where email = @login and senha = @senha";
            cmd.Parameters.AddWithValue("@login", login);
            cmd.Parameters.AddWithValue("@senha", senha);
            cmd.Connection = c.conectar();
            SqlDataAdapter data = new SqlDataAdapter(); //pega dados da tabela
            data.SelectCommand = cmd;
            DataTable dt = new DataTable(); //tabela que recebe dados
            data.Fill(dt); //preenche a tabela com os dados do adapter

            if (dt != null && dt.Rows.Count > 0)
            {
                Usuario f = new Usuario();
                f.Id = Convert.ToInt32(dt.Rows[0]["id"]);
                f.Email = dt.Rows[0]["email"].ToString();
                f.Nome = dt.Rows[0]["nome"].ToString();
             
                return f;
            }
            else
                return null;
        }

        public void cadastrarMaterial (String descr, int estoque, String status)
        {
            cmd.CommandText = "exec cadastroMaterial @descr, @estq, @status";

            cmd.Parameters.AddWithValue("@descr", descr);
            cmd.Parameters.AddWithValue("@estq", estoque);
            cmd.Parameters.AddWithValue("@status", status);

            try
            {
                cmd.Connection = c.conectar();
                cmd.ExecuteNonQuery();
                msg = "Cadastrado com sucesso!";
                c.encerrar();
            }
            catch (SqlException e)
            {
                msg = "Erro ao conectar-se";
                throw e;
            }

        }
        public int requisitar(String prof, int ra)
        {
            int i = 0;
            string sql = "declare @cod int; exec requisitar "+prof+", "+ra+", @cod output; select @cod as 'id'";
            SqlCommand comando = new SqlCommand(sql, c.conectar());
            SqlDataAdapter data = new SqlDataAdapter(); //pega dados da tabela
            data.SelectCommand = comando;
            DataTable table = new DataTable(); //tabela que recebe dados
            data.Fill(table); //preenche a tabela com os dados do adapter
            IEnumerable consulta = from aluno in table.AsEnumerable()
                                   select aluno;
            foreach (DataRow dr in consulta)
            {
                // lbDados.Items.Add(dr.Field<int>("AlunoId") + " - " + dr.Field<string>("Nome"));
                i = dr.Field<int>("id");
            }
            c.encerrar();

            
            return i;
        }
        
        public void addItem(int requisicao, int material, int qtd)
        {
            cmd.CommandText = "exec addItem @req, @mat, @qtd, @mg output";

            cmd.Parameters.AddWithValue("@req", requisicao);
            cmd.Parameters.AddWithValue("@mat", material);
            cmd.Parameters.AddWithValue("@qtd", qtd);

            try
            {
                cmd.Connection = c.conectar();
                cmd.ExecuteNonQuery();
                msg = "Cadastrado com sucesso!";
                c.encerrar();
            }
            catch (SqlException e)
            {
                msg = "Erro ao conectar-se";
                throw e;
            }
        }
        public void deletarAluno(int ra)
        {
            c.conectar();
            cmd.CommandText = "delete from aluno where ra = @ra";

            cmd.Parameters.AddWithValue("@ra", ra);

            try
            {
                cmd.Connection = c.conectar();
                cmd.ExecuteNonQuery();
               
                c.encerrar();
            }
            catch (SqlException e)
            {
                msg = "Erro ao conectar-se";
                throw e;
            }
        }

        public void editarAluno(int ra, String name, String email)
        {
            c.conectar();
            cmd.CommandText = "update aluno set nome = @name, email = @email where ra = @ra";

            cmd.Parameters.AddWithValue("@ra", ra);
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@email", email);

            try
            {
                cmd.Connection = c.conectar();
                cmd.ExecuteNonQuery();

                c.encerrar();
            }
            catch (SqlException e)
            {
                msg = "Erro ao conectar-se";
                throw e;
            }
        }
        public IEnumerable<AlunoViewModel> listarAluno()
        {
            try
            { //estrutura que faz tratamento de erros
                
                string sql = "SELECT * FROM aluno";
                SqlCommand comando = new SqlCommand(sql, c.conectar());
                SqlDataAdapter data = new SqlDataAdapter(); //pega dados da tabela
                data.SelectCommand = comando;
                DataTable table = new DataTable(); //tabela que recebe dados
                data.Fill(table); //preenche a tabela com os dados do adapter
                c.encerrar();
                IList<AlunoViewModel> alunos = new List<AlunoViewModel>();
                foreach(DataRow linha in table.Rows)
                {
                    AlunoViewModel al = new AlunoViewModel();
                    al.Ra = Convert.ToInt32(linha["ra"]);
                    al.Nome = linha["nome"].ToString();
                    al.Email = linha["email"].ToString();
                    alunos.Add(al);

                }
                return alunos;
            }


            catch (Exception erro)
            {
                throw erro;
            }
        }
        public AlunoViewModel listarAlunoRA(int ra)
        {   string sql = "SELECT * FROM aluno where ra = " + ra;
                SqlCommand comando = new SqlCommand(sql, c.conectar());
                SqlDataAdapter data = new SqlDataAdapter(); //pega dados da tabela
                data.SelectCommand = comando;
                DataTable table = new DataTable(); //tabela que recebe dados
                data.Fill(table); //preenche a tabela com os dados do adapter
                c.encerrar();
                AlunoViewModel al = new AlunoViewModel();

                al.Ra = Convert.ToInt32(table.Rows[0]["ra"]);
                al.Nome = table.Rows[0]["nome"].ToString();
                al.Email = table.Rows[0]["email"].ToString();
                return al;
            
        }
    }
}