﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LabControl.Models
{
    public class AlunoViewModel
    {
        [Required(ErrorMessage = "RA é obrigatório")]
        [DataType(DataType.Text)]
        [RegularExpression(@"^[A-Za-z0-9]*\d+[A-Za-z0-9]*$", ErrorMessage ="RA são apenas números")]
        public int Ra { get; set; }
        public String Nome { get; set; }

        [Required(ErrorMessage = "E-mail é obrigatório")]
        [DataType(DataType.EmailAddress)]
        public String Email { get; set; }

        

        public AlunoViewModel()
        {

        }
        public AlunoViewModel(int ra, String nome, String email)
        {
            this.Ra = ra;
            this.Nome = nome;
            this.Email = email;
        }

    }
}