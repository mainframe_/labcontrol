﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LabControl.Models
{
    public class Usuario
    {
        
        [DataType(DataType.Text)]
        [RegularExpression(@"^[A-Za-z0-9]*\d+[A-Za-z0-9]*$", ErrorMessage = "RA são apenas números")]
        public int Id { get; set; }
        public String Nome { get; set; }

        [Required(ErrorMessage = "E-mail é obrigatório")]
        [DataType(DataType.EmailAddress)]
        public String Email { get; set; }
        [Required(ErrorMessage ="Senha é obrigatória")]
        [DataType(DataType.Password)]
        public String Senha { get; set; }
        
        public String TipoUser { get; set; }



    }

}