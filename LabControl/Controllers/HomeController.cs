﻿using LabControl.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LabControl.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            Session["autenticado"] = null;
            return View();
        }
        [HttpPost]
        public ActionResult Index(Usuario usuario)
        {

            if (!ModelState.IsValid)
                return View(usuario);

            Comandos cm = new Comandos();
            Usuario logado = cm.Autenticar(usuario.Email, usuario.Senha);
            if ( logado != null)
            {
                Session["autenticado"] = "OK";
                Session["idUsuario"] = logado.Id;
                Session["nomeUsuario"] = logado.Nome;
                Session["tipoUsuario"] = logado.TipoUser;
                return RedirectToAction("Index", "Aluno");

            }


            return View(usuario);

        }
        
        public ActionResult About(Usuario us)
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }
        public ActionResult Contact(Usuario us)
        {
            if (us == null)
                return RedirectToAction("Index");
            ViewBag.Message = "Your contact page."+us.Nome+"OI";

            return View();
        }
        
    }
}