﻿using LabControl.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LabControl.Controllers
{
    public class AlunoController : Controller
    {
        // POST: Aluno
        
        public ActionResult Index()
        {
            Comandos aluno = new Comandos();
            
            return View(aluno.listarAluno());
        }
        [HttpGet]
        public ActionResult Inserir()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Inserir(AlunoViewModel aluno)
        {
           

            if (!ModelState.IsValid)
                return View(aluno);

            Comandos cm = new Comandos();

            cm.cadastrarAluno(aluno.Ra, aluno.Nome, aluno.Email);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int ra)
        {
            Comandos c = new Comandos();
            AlunoViewModel aluno = c.listarAlunoRA(ra);
            return View(aluno);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirma(int ra)
        {
            Comandos cm = new Comandos();

            cm.deletarAluno(ra);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit (int ra)
        {
            Comandos c = new Comandos();
            AlunoViewModel aluno = c.listarAlunoRA(ra);
            return View(aluno);
        }
        [HttpGet]
        public ActionResult Details(int ra)
        {
            Comandos c = new Comandos();
            AlunoViewModel aluno = c.listarAlunoRA(ra);
            return View(aluno);
        }

        [HttpPost, ActionName("Edit")]
        public ActionResult EditConfirma(AlunoViewModel aluno)
        {


            Comandos cm = new Comandos();
            cm.editarAluno(aluno.Ra, aluno.Nome, aluno.Email);
            return RedirectToAction("Index");
        }
    }
}